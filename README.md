
WP Backup v0.3
==============

This script is intended to be used as a cron backup script for WP installations,
compatible with Rackspace Cloud Sites. These features have been implemented:

 * Dump current database to temp folder using credentials from wp-config.php
 * Create dated .tar.gz file of site root and database
 * Push archive to Amazon S3
 * Delete archive locally, if set.

Usage
-----

 1. Upload wpbackup/ *outside* the site root. (An .htaccess file prevents reading
    of the directory, but be safe. Database dumps and S3 credentials are
    stored there)
    
    * If using the general Rackspace deployment as described in config.default.sh,
      install to /www.site.com/wpbackup/
      
 2. Edit necessary variables in config.[profile].sh using config.default.sh as
    a template.
    
    * If only one profile will be used in this installation, store as config.sh.
    
 3. Execute `./wpbackup` or `./wpbackup.sh -c profile` as necessary to test
 
 4. Install as cron job or an RSCS Scheduled Task

