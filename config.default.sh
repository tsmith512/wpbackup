#!/bin/bash

############################################################
# Site source settings                                     #
############################################################

# Absolute, please. If a relative path is used, it should be
# relative to the directory where wpbackup.sh is stored
SITE_ROOT="./test-files/public_html/"

# Profile name, included in the archive name and folder, and
# used to identify the proper AWS Secret Key file.
#
# Since this script was originally designed to backup sites
# on Rackspace Cloud Sites, use the second line (commented)
# to automatically set the SITE_NAME from the "www.site.com"
# directory. Note that this *requires* that the script sits
# in /www.site.com/wpbackup/ to pull the directory name
# correctly.
#
# Using the Rackspace line with generic/unlabled 'config.sh'
# and 's3.key' files will allow a set of fixed files for
# general deployment.
SITE_NAME="default"
#SITE_NAME=$( basename $( dirname $( cd -P "$( dirname "${BASH_SOURCE[0]}" )" && pwd )))

############################################################
# Amazon S3 Settings                                       #
############################################################
# NOTE: Anyone who can read this file can get these access
# credentials. It would be best to use credentials of an
# account with an ACL that allows only "PutObject".
S3_ACCESS="00000000000000000000"

# S3 Object Path as '/bucket/folder' without trailing slash.
# Note that a folder named {SITE_NAME} will be created at the
# end  of this path so like archives are stored together.
S3_PATH="/bucket"

# You must also create an 's3.[SITE_NAME].key' file
# containing the 40 character Secret Key. If that key is not
# available, 's3.key' will be used. Remember to leave no
# trailing whitespace. This is a requirement of s3-bash by
# Raphael James Cohn which handles the upload. Filesize is
# tested to match eactly 40 bytes by that script.

# Do we delete the resulting archive? (Tests for 'y')
S3_PUTCLEAN="y"

############################################################
# Database Overrides                                       #
############################################################
# Source DB defaults taken from <site-root>/wp-config.php
# Overide here as necessary
DB_NAME=""
DB_USER=""
DB_PASSWORD=""
DB_HOST=""

