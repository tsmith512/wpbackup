#!/usr/bin/env bash

############################################################
# WPBACKUP by Taylor Smith v0.3                            #
#                                                          #
# Automatically back up WP installations and their DBs to  #
# Amazon S3. Originally written to accomodate Rackspace    #
# Cloud Sites but will work on any system that has these   #
# requirements:                                            #
# For this script: bash, mysqldump, wget, grep, tar, gzip  #
# For s3-bash: openssl, curl, od, dd, printf, sed, awk,    #
#   sort, mktemp, env, bash. (Although I think not all of  #
#   these are actually used, s3-bash does test for them.)  #
############################################################

############################################################
# Parse Arguments and Flags                                #
############################################################

CONFIG="config.sh"

while getopts c: option; do
	case "${option}"
	in
		c) CONFIG="config.${OPTARG}.sh";;
	esac
done

############################################################
# Check for our requirements                               #
############################################################

declare -a deps=(mysqldump wget grep tar gzip)

for i in ${deps[@]}; do
	type -P $i >/dev/null 2>&1 || { 
		echo >&2 "${i} not available. Aborting.";
		exit 1;
	}
done

source ${0%/*}/$CONFIG > /dev/null 2>&1 || {
	echo >&2 "Config file not found at: ${0%/*}/${CONFIG}";
	exit 1;
}

############################################################
# Set up variables                                         #
############################################################

# We change directories for Tar and any relative paths are
# relative to the location where the script it stored.
# Figure that out and go there.
MY_DIR="$( cd -P "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd ${MY_DIR}

# This is the temp storage directory for db dump and archives
TEMP_DIR=${MY_DIR}/temp

# The date format to be included in the file name
DATE=$(date +"%Y_%m_%d")

# The names of the dump and archive files
DB_DUMP="${SITE_NAME}_${DATE}.bak.sql"
ARCHIVE="${SITE_NAME}_${DATE}.bak.tar"

############################################################
# Source wp-config.php Database Variables                  #
############################################################
if [ -e $SITE_ROOT/wp-config.php ]; then
	eval $(awk -F"[()']" '/^define/{printf "%s=\"%s\"\n", $3, $5;}' < ${SITE_ROOT}/wp-config.php | grep "DB_" | sed s/DB_/WPCDB_/g )

	[[ -z $DB_NAME ]] && DB_NAME=$WPCDB_NAME
	[[ -z $DB_USER ]] && DB_USER=$WPCDB_USER
	[[ -z $DB_PASSWORD ]] && DB_PASSWORD=$WPCDB_PASSWORD
	[[ -z $DB_HOST ]] && DB_HOST=$WPCDB_HOST
fi

############################################################
# Database Dump                                            #
############################################################
if [ ! -d "$TEMP_DIR" ]; then
	mkdir ${TEMP_DIR}
fi

mysqldump -h ${DB_HOST} \
	--user=${DB_USER} \
	--password=${DB_PASSWORD} \
	--add-drop-table --compact \
	--complete-insert --extended-insert --create-options \
	${DB_NAME} > ${TEMP_DIR}/${DB_DUMP}

############################################################
# Build Archive                                            #
############################################################

# Tar will include the path on the command line so let's
# pick up the database dump file in its location.
cd ${TEMP_DIR}

# Create the archive, add the database dump, and delete it
tar -cf ${ARCHIVE} ${DB_DUMP}
rm ${DB_DUMP}

# Jump over to the site root and add those files. We'll do
# this in a separate directory so the database dump is never
# stored within the site root.
cd ${MY_DIR}
cd ${SITE_ROOT}
tar -rf ${TEMP_DIR}/${ARCHIVE} *

# We couldn't have Tar gzip this file for us because it was
# appended, so let's do that now, then clean up a bit.
cd ${TEMP_DIR}
gzip --best -f ${ARCHIVE}
ARCHIVE="${SITE_NAME}_${DATE}.bak.tar.gz"

############################################################
# Upload to S3                                             #
############################################################
cd ${MY_DIR}

S3_KEY="s3.${SITE_NAME}.key"

if [ ! -e $S3_KEY ]; then
	S3_KEY="s3.key"
fi

/usr/bin/env bash ./lib/s3-put -S -k ${S3_ACCESS} \
	-s ${MY_DIR}/${S3_KEY} \
	-T ${TEMP_DIR}/${ARCHIVE} \
	"${S3_PATH}/${SITE_NAME}/${ARCHIVE}"

[ "$S3_PUTCLEAN" != "y" ] ||
	rm ${TEMP_DIR}/${ARCHIVE};
